package service;

import protocol.dto.*;
import protocol.type.BroadCastType;
import service.domian.Client;
import protocol.dto.Pair;
import service.domian.Room;
import protocol.dto.Text;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Service {
    private static final int RESULT_SUCCESS = 0;
    private static final int UNKNOWN_ERROR = 1;
    private static final int UNKNOWN_COMMAND = 2;
    private static final int NOT_ACCESSIBLE = 3;
    private static final int RESULT_ALREADY_LOG_IN = 4;
    private static final int NO_FILE = 5;

    private static Service instance = new Service();

    private final ServerState serverState = new ServerState();

    public static Service getInstance() {
        return instance;
    }

    private RequestResponse successResponse(Request request, Object data) {
        Client client = serverState.getUser(request.getUserId());
        RequestResponse response = new RequestResponse(request.getRequestId(), request.getRequestType(), RESULT_SUCCESS, data, client.getIp());

        return response;
    }

    private RequestResponse failResponse(Request request, int FAIL_NUMBER) {
        Client client = serverState.getUser(request.getUserId());
        RequestResponse response = new RequestResponse(request.getRequestId(), request.getRequestType(), FAIL_NUMBER, null, client.getIp());

        return response;
    }

    private BroadCastResponse broadCastResponse(BroadCastType broadCastType, String sender, Room room, Object data) {
        List ipList = room.getUserSet().stream()
                .map(id -> serverState.getUser(id).getIp())
                .collect(Collectors.toList());

//        List<String> logInIpList = room.getUserSet().stream()
//                .filter(id -> serverState.getUser(id).isState())
//                .map(id -> serverState.getUser(id).getIp())
//                .collect(Collectors.toList());
//
//        List<String> logOutIdList = room.getUserSet().stream()
//                .filter(id -> !serverState.getUser(id).isState())
//                .collect(Collectors.toList());

        BroadCastResponse broadCastResponse = new BroadCastResponse(broadCastType, room.getRoomNumber(), sender, data, ipList);

        return broadCastResponse;
    }

    public Response login(Request request) {
        serverState.createUser(request.getUserId(), request.getIp());

        Response response = new Response(successResponse(request, null));

        return response;
    }

    public Response logOut(Request request) {
        Room room = serverState.getRoom(request.getRoomNumber());
        Response response = new Response(successResponse(request, null), broadCastResponse(BroadCastType.SOCKETCLOSE, request.getUserId(), room, null));

        return response;
    }

    public Response createRoom(Request request) {
        Room room = serverState.createRoom((String) request.getData(), request.getUserId());

        Response response = new Response(successResponse(request, room.getRoomNumber()));

        return response;
    }

    public Response showRoomList(Request request) {
        Client client = serverState.getUser(request.getUserId());
        List<Integer> userRoomNumberList = client.getRoomList();
        List<RoomDto> userRoomList = new ArrayList<>();

        userRoomNumberList.forEach(roomNumber -> {
            Room room = serverState.getRoom(roomNumber);
            userRoomList.add(new RoomDto(room.getRoomNumber(), room.getRoomId(), room.getNumOfUser(), room.getNumOfUnread(client.getId())));
        });

        Response response = new Response(successResponse(request, userRoomList));

        return response;
    }

    public Response enterRoom(Request request) {
        Client client = serverState.getUser(request.getUserId());
        client.setCurrentRoomNumber(request.getRoomNumber());
        Room room = serverState.getRoom(request.getRoomNumber());

        int textStart = room.getNumOfRead(request.getUserId());
        int textEnd = room.getTextId();
        Pair pair = new Pair(textStart, textEnd);

        room.reading(request.getUserId());

        Response response = new Response(successResponse(request, null), broadCastResponse(BroadCastType.ENTERROOM, request.getUserId(), room, pair));

        return response;
    }

    public Response inviteUser(Request request) {
        String[] users = (String[]) request.getData();

        Room room = serverState.getRoom(request.getRoomNumber());

        Arrays.stream(users).forEach(id -> {
            Client client = serverState.getUser(id);
            //client.setCurrentRoomNumber(request.getRoomNumber());  invite 할 때 방으로 바로 이동하는 거
            client.addRoomList(request.getRoomNumber());
        });

        room.addUser(users);

        Response response = new Response(successResponse(request, null), broadCastResponse(BroadCastType.INVITE, request.getUserId(), room, users));

        return response;
    }

    public Response sendMessage(Request request) {
        Room room = serverState.getRoom(request.getRoomNumber());
        int textId = room.getTextId();

        Set<String> readUserSet = room.getUserSet().stream().filter(id ->
                serverState.getUser(id).isLogin() && serverState.getUser(id).getCurrentRoomNumber() == room.getRoomNumber()).collect(Collectors.toSet());
        int numOfRead = readUserSet.size();

        String message = (String) request.getData();
        Text text = new Text(textId, room.getNumOfUser() - numOfRead, message.length(), message);

        room.addText(text);
        readUserSet.stream().forEach(id -> room.reading(id));

        Response response = new Response(successResponse(request, null), broadCastResponse(BroadCastType.SEND, request.getUserId(), room, text));

        return response;
    }

    public Response quitRoom(Request request) {
        Client client = serverState.getUser(request.getUserId());
        Room room = serverState.getRoom(request.getRoomNumber());

        client.setCurrentRoomNumber(-1);
        client.getRoomList().remove((Integer) request.getRoomNumber());
        room.getUserSet().remove(client.getId());

        Response response = new Response(successResponse(request, null), broadCastResponse(BroadCastType.QUIT, client.getId(), room, null));

        return response;
    }

    public Response exitRoom(Request request) {
        Client client = serverState.getUser(request.getUserId());

        client.setCurrentRoomNumber(-1);

        Response response = new Response(successResponse(request, null));

        return response;
    }

    public Response socketDisconnected(String ip) {
        Client client = serverState.getUserByIp(ip);

        List<String> ipList = new ArrayList<>();

        client.getRoomList().stream()
                .map(roomNumber -> serverState.getRoom(roomNumber))
                .forEach(room -> ipList.addAll(room.getUserSet()));

        return new Response(broadCastResponse(BroadCastType.SOCKETCLOSE, client.getId(), serverState.getRoom(client.getCurrentRoomNumber()), null));
    }
}
