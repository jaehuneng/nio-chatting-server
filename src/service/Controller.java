package service;

import protocol.dto.BroadCastResponse;
import protocol.dto.Request;
import protocol.dto.Response;

public class Controller {
    private static Service service = new Service();

    public static Response doService(Request request) {
        switch (request.getRequestType()) {
            case LOGIN: return service.login(request);
            case CREATEROOM: return service.createRoom(request);
            case ROOMLIST: return service.showRoomList(request);
            case ENTER: return service.enterRoom(request);
            case INVITE: return service.inviteUser(request);
            case SEND: return service.sendMessage(request);
            case QUIT: return service.quitRoom(request);
            case EXIT: return service.exitRoom(request);
            case LOGOUT: return service.logOut(request);

            default: throw new IllegalArgumentException("잘못된 Request Type");
        }
    }

    public static Response broadCastSocketDisconnected(String ip) {
        return service.socketDisconnected(ip);
    }
}
