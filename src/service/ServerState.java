package service;

import protocol.dto.BroadCastResponse;
import service.domian.Client;
import service.domian.Room;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ServerState {
    private HashMap<String, Client> userTable = new HashMap<>();
    private HashMap<Integer, Room> roomTable = new HashMap<>();
    private HashMap<String, List<BroadCastResponse>> broadCastTable = new HashMap<>();

    private int roomNumber = 0;

    public Room createRoom(String roomId, String userId) {
        Room room = new Room(roomId, roomNumber);

        room.addUser(userId);

        userTable.get(userId).addRoomList(roomNumber);

        roomTable.put(roomNumber ,room);
        roomNumber++;
        return room;
    }

    public Room getRoom(int roomNumber) {
        return roomTable.get(roomNumber);
    }

    public Client createUser(String userId, String ip) {
        Client client = null;
        if (userTable.containsKey(userId)) {
            client = userTable.get(userId);
            client.setState(Client.State.NOTCHATTING);
            client.setIp(ip);
        } else {
            client = new Client(userId, ip);
            userTable.put(userId, client);
        }

        return client;
    }

    public Client getUser(String userId) {
        Client client = userTable.get(userId);

        return client != null ? client : createUser(userId, "");
    }

    public Client getUserByIp(String ip) {
        for (Client client : userTable.values()) {
            if (client.getIp() == ip) {
                return client;
            }
        }
        return null;
    }

    public void addBroadCast(String userId, BroadCastResponse broadCastResponse) {
        if (broadCastTable.get(userId) == null) {
            broadCastTable.put(userId, new ArrayList<>());
        }
        broadCastTable.get(userId).add(broadCastResponse);
    }
}
