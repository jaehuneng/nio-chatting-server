package service.domian;

import protocol.dto.BroadCastResponse;
import protocol.dto.Response;
import protocol.dto.Text;

import java.util.*;

public class Room {
    private int roomNumber;
    private String roomId;
    private List<Text> textList;
    private HashMap<String, Integer> read;

    private Set<String> userSet;

    public Room(String roomId, int roomNumber) {
        this.roomNumber = roomNumber;
        this.roomId = roomId;
        this.userSet = new HashSet<>();
        this.textList = new ArrayList<>();
        this.read = new HashMap<>();
    }

    public Set<String> getUserSet() {
        return userSet;
    }

    public String getRoomId() {
        return roomId;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void addUser(String id) {
        userSet.add(id);
        read.put(id, getTextId());
    }

    public void addUser(String[] ids) {
        Arrays.stream(ids).forEach(id -> addUser(id));
    }

    public int getNumOfUser() {
        return this.userSet.size();
    }

    public void addText(Text text) {
        //userSet.stream().forEach(id -> read.replace(id, read.get(id) + 1));
        this.textList.add(text);
    }

    public int getTextId() {
        return textList.size();
    }

    public void reading(String userId) {
        read.replace(userId, getTextId());
    }

    public int getNumOfRead(String userId) {
        return read.get(userId);
    }

    public int getNumOfUnread(String userId) {
        return getTextId() - read.get(userId);
    }
}
