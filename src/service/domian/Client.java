package service.domian;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private String id;
    private String ip;
    private int currentRoomNumber;
    private List<Integer> roomList;
    private State state;

    public Client(String id, String ip) {
        this.id = id;
        this.ip = ip;
        state = State.NOTCHATTING;
        currentRoomNumber = -1;
        roomList = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public boolean isLogin() {
        if (state == State.CHATTING || state == State.NOTCHATTING) {
            return true;
        }
        return false;
    }

    public State getState(int roomNumber) {
        if (state == State.LOGOUT || state == State.DISCONNECTED) {
            return state;
        }
        if (currentRoomNumber == roomNumber){
            return State.CHATTING;
        }
        return State.NOTCHATTING;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Integer> getRoomList() {
        return roomList;
    }

    public void addRoomList(int roomNumber) {
        this.roomList.add(roomNumber);
    }

    public int getCurrentRoomNumber() {
        return currentRoomNumber;
    }

    public void setCurrentRoomNumber(int currentRoomNumber) {
        this.currentRoomNumber = currentRoomNumber;
    }

    public enum State {
        LOGOUT(0), CHATTING(1), NOTCHATTING(2), DISCONNECTED(3);
        private int state;
        State(int state) {
            this.state = state;
        }
    }
}