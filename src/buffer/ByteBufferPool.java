package buffer;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class ByteBufferPool {
    private static final int MEMORY_BLOCKSIZE = 1024;
    private static final int FILE_BLOCKSIZE = 2048;

    private final ArrayList<ByteBuffer> memoryQueue = new ArrayList();

    private boolean isWait = false;

    public void initMemoryBuffer(int size) {
        int bufferCount = size / MEMORY_BLOCKSIZE;
        size = bufferCount * MEMORY_BLOCKSIZE;
        ByteBuffer directBuffer = ByteBuffer.allocateDirect(size);
    }

    private void divideBuffer (ByteBuffer buffer, int blockSize, ArrayList list) {
        int bufferCount = buffer.capacity() / blockSize;
        int position = 0;
        for (int i=0; i < bufferCount; i++) {
            int max = position + blockSize;
            buffer.limit(max);
            list.add(buffer.slice());
            position = max;
            buffer.position(position);
        }
    }

    private ByteBuffer getBuffer(ArrayList firstQueue, ArrayList secondQueue) {
        ByteBuffer byteBuffer = getBuffer(firstQueue, false);
        if (byteBuffer == null) {
            byteBuffer = getBuffer(secondQueue, false);
            if (byteBuffer == null) {
                if (isWait) {
                    byteBuffer = getBuffer(firstQueue, true);
                } else {
                    byteBuffer = ByteBuffer.allocate(MEMORY_BLOCKSIZE);
                }
            }
        }
        return byteBuffer;
    }

    private ByteBuffer getBuffer(ArrayList<ByteBuffer> queue, boolean wait) {
        synchronized (queue) {
            if (queue.isEmpty()) {
                if (wait) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    return null;
                }
            }
        }
        return queue.remove(0);
    }

    public void putBuffer(ByteBuffer buffer) {
        if (buffer.isDirect()) {
            switch (buffer.capacity()) {
                case MEMORY_BLOCKSIZE:
                    putBuffer(buffer, memoryQueue);
                    break;
            }
        }
    }

    private void putBuffer(ByteBuffer buffer, ArrayList<ByteBuffer> queue) {
        buffer.clear();
        synchronized (queue) {
            queue.add(buffer);
            queue.notify();
        }
    }

    public synchronized void setWait(boolean wait) {
        this.isWait = wait;
    }

    public synchronized boolean isWait() { return isWait; }
}
