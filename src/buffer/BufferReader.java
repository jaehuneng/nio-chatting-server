package buffer;

import protocol.dto.*;
import protocol.type.BroadCastType;
import protocol.type.RequestType;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class BufferReader {
    private static Charset charset = Charset.forName("UTF-8");

    public static Request readRequestBuffer(ByteBuffer byteBuffer) {
        int requestId = byteBuffer.getInt(0);

        RequestType requestType = RequestType.findRequestType(byteBuffer.getInt(4));
        String id = getString(byteBuffer, 8, 23);
        int roomNumber = byteBuffer.getInt(24);
        Object data = getRequestData(byteBuffer, 28, requestType);

        return new Request(requestId, requestType, id, roomNumber, data);
    }

    public static RequestResponse readResponseBuffer(ByteBuffer byteBuffer) {
        int requestId = byteBuffer.getInt(0);

        RequestType requestType = RequestType.findRequestType(byteBuffer.getInt(4));
        int result = byteBuffer.getInt(8);
        Object data = getResponseData(byteBuffer, 12, requestType);

        return new RequestResponse(requestId, requestType, result, data);
    }

    public static BroadCastResponse readBroadCastBuffer(ByteBuffer byteBuffer) {
        int broadCastNumber = byteBuffer.getInt(4);
        int roomNumber = byteBuffer.getInt(8);
        String curId = getString(byteBuffer, 12, 27);

        BroadCastType broadCastType = BroadCastType.findBroadCastType(broadCastNumber);

        String time = getString(byteBuffer, 28, 39);
        Object data = getBroadCastData(byteBuffer, 40, broadCastType);

        return new BroadCastResponse(broadCastType, roomNumber, curId, data);
    }

    public static String getString(ByteBuffer byteBuffer, int start, int end) {
        byteBuffer.position(start);

        for (int i=start; i<end; i++) {
            if (byteBuffer.get(i) == 0) {
                end = i;
                break;
            }
        }

        byteBuffer.limit(end);
        String result = charset.decode(byteBuffer).toString();

        byteBuffer.limit(1000);
        return result;
    }

    private static Object getRequestData(ByteBuffer byteBuffer, int start, RequestType requestType) {
        switch (requestType) {
            case LOGIN: return null;
            case LOGOUT: {

            }
            case SEND: {
                return getString(byteBuffer, start, 1000);
            }
            case FILEUPLOAD: return null;
            case INVITE: {
                int length = byteBuffer.getInt(start);
                String[] data = new String[length];

                int index = start + 4;
                for (int i=0; i<length; i++) {
                    data[i] = getString(byteBuffer, index, index + 16);
                    index += 16;
                }

                return data;
            }
            case CREATEROOM:
                return getString(byteBuffer, start, start + 16);
            case ROOMLIST: {
                return null;
            }
            case ENTER: {
                return byteBuffer.getInt(start);
            }
            case FILEREGIST:
            case QUIT: return null;
            case EXIT: return null;

            default: throw new IllegalArgumentException("잘못된 requestType!");
        }
    }

    private static Object getResponseData(ByteBuffer byteBuffer, int start, RequestType requestType) {
        switch (requestType) {
            case LOGIN:
            case LOGOUT:
            case SEND:
            case QUIT:
            case EXIT:
            case FILEUPLOAD:
                return null;
            case CREATEROOM: {
                return byteBuffer.getInt(start);
            }

            case ROOMLIST: {
                int numOfRoom = byteBuffer.getInt(start);
                RoomDto[] rooms = new RoomDto[numOfRoom];

                int index = start + 4;
                for (int i=0; i<numOfRoom; i++) {
                    int roomNumber = byteBuffer.getInt(index);
                    String roomId = getString(byteBuffer, index + 4, index + 20);
                    int numOfUser = byteBuffer.getInt(index + 20);
                    int numOfText = byteBuffer.getInt(index + 24);
                    rooms[i] = new RoomDto(roomNumber, roomId, numOfUser, numOfText);
                    index += 28;
                }

                return rooms;
            }
            case ENTER: {
                return null;
            }

            case INVITE: {
                return null;
            }
            case FILEREGIST:

            default: throw new IllegalArgumentException("잘못된 requestType!");
        }
    }

    private static Object getBroadCastData(ByteBuffer byteBuffer, int start, BroadCastType broadCastType) {
        switch (broadCastType) {
            case INVITE: {
                int num = byteBuffer.getInt(start);
                String[] data = new String[num];

                int index = start + 4;
                for (int i=0; i<num; i++) {
                    data[i] = getString(byteBuffer, index, index+16);
                    index += 16;
                }

                return data;
            }

            case QUIT: {
                return null;
            }

            case SEND: {
                printByteBuffer(byteBuffer);
                int textId = byteBuffer.getInt(start);
                int numOfRead = byteBuffer.getInt(start + 4);
                int length = byteBuffer.getInt(start + 8);
                String message = getString(byteBuffer, start + 12, start + 12 + length);
                return new Text(textId, numOfRead, length, message);
            }
            case FILEUPLOAD: {

            }
            case FILEREMOVE: {

            }
            case ENTERROOM: {
                return null;
            }
            default: throw new IllegalArgumentException("없는 broadcastNumber!");
        }
    }

    public static void printByteBuffer(ByteBuffer byteBuffer) {
        for (int i=0; i<byteBuffer.limit(); i++) {
            System.out.print((int)byteBuffer.get(i) + " ");
        }
        System.out.println();
    }
}
