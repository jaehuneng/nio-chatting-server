package protocol.type;

public enum RequestType {
    LOGIN(0),
    LOGOUT(1),
    SEND(2),
    FILEUPLOAD(3),
    FILELIST(4),
    FILEDOWNLOAD(5),
    FILEREMOVE(6),
    CREATEROOM(7),
    QUIT(8),
    INVITE(9),
    USERLIST(10),
    ROOMLIST(11),
    ENTER(12),
    FILEREGIST(13),
    FILEINFO(14),
    EXIT(15);

    private int requestNumber;

    RequestType(int requestNumber) {
        this.requestNumber = requestNumber;
    }

    public int getRequestNumber() {
        return this.requestNumber;
    }

    public static RequestType findRequestType(int requestNumber) {
         for(RequestType requestType : RequestType.values()) {
             if (requestType.requestNumber == requestNumber) {
                 return requestType;
             }
         }
         throw new IllegalArgumentException("잘못된 request가 들어옴!");
    }
}
