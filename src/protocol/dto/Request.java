package protocol.dto;

import protocol.type.RequestType;

public class Request {
    private int requestId;
    private RequestType requestType;
    private String userId;
    private int roomNumber;
    private Object data;
    private String ip;

    public Request(int requestId, RequestType requestType, String userId, int roomNumber, Object data) {
        this.userId = userId;
        this.requestId = requestId;
        this.requestType = requestType;
        this.roomNumber = roomNumber;
        this.data = data;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getRequestId() {
        return requestId;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public Object getData() {
        return data;
    }

    public String getUserId() {
        return userId;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestId=" + requestId +
                ", requestType=" + requestType +
                ", userId='" + userId + '\'' +
                ", roomNumber=" + roomNumber +
                ", data=" + data +
                ", ip='" + ip + '\'' +
                '}';
    }
}
