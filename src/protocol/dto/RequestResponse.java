package protocol.dto;

import protocol.type.RequestType;

public class RequestResponse {
    private int requestId;
    private RequestType requestType;
    private int result;
    private Object data;
    private String ip;

    public RequestResponse(int requestId, RequestType requestType, int result, Object data, String ip) {
        this.requestId = requestId;
        this.requestType = requestType;
        this.result = result;
        this.data = data;
        this.ip = ip;
    }

    public RequestResponse(int requestId, RequestType requestType, int result, Object data) {
        this.requestId = requestId;
        this.requestType = requestType;
        this.result = result;
        this.data = data;
        this.ip = null;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public int getRequestId() {
        return requestId;
    }

    public int getResult() {
        return result;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "RequestResponse{" +
                "requestId=" + requestId +
                ", requestType=" + requestType +
                ", result=" + result +
                ", data=" + data +
                '}';
    }
}
