package protocol.dto;

public class Pair {
    private int x;
    private int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
        if(x == y) {
            this.x = -1;
            this.y = -1;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
