package protocol.dto;

import protocol.type.BroadCastType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class BroadCastResponse {

    private BroadCastType broadCastType;
    private int roomNumber;
    private String sender;
    private Object data;
    private List<String> ipList;
    private String time;

    public BroadCastResponse(BroadCastType broadCastType, int roomNumber, String curId, Object data, List ipList) {
        this.broadCastType = broadCastType;
        this.roomNumber = roomNumber;
        this.sender = curId;
        this.data = data;
        this.ipList = ipList;
        this.time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddHHmmss"));
    }

    public BroadCastResponse(BroadCastType broadCastType, int roomNumber, String curId, Object data) {
        this.broadCastType = broadCastType;
        this.roomNumber = roomNumber;
        this.sender = curId;
        this.data = data;
        this.time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddHHmmss"));
    }

    public List<String> getIpList() {
        return ipList;
    }

    public void setIpList(List<String> ipList) {
        this.ipList = ipList;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public BroadCastType getBroadCastType() {
        return broadCastType;
    }

    public void setBroadCastType(BroadCastType broadCastType) {
        this.broadCastType = broadCastType;
    }

    public String getCurId() {
        return sender;
    }

    public void setCurId(String curId) {
        this.sender = curId;
    }

    @Override
    public String toString() {
        return "BroadCastResponse{" +
                "broadCastType=" + broadCastType +
                ", roomNumber=" + roomNumber +
                ", curId='" + sender + '\'' +
                ", data=" + data +
                ", ipList=" + ipList +
                '}';
    }
}
