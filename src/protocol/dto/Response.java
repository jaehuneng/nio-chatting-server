package protocol.dto;

import java.util.function.Consumer;

public class Response {
    private RequestResponse requestResponse;
    private BroadCastResponse broadCastResponse;
    private Consumer callback;

    public Response(RequestResponse response) {
        this.requestResponse = response;
    }

    public Response(BroadCastResponse response) {
        this.broadCastResponse = response;
    }

    public Response(RequestResponse requestResponse, BroadCastResponse broadCastResponse) {
        this.requestResponse = requestResponse;
        this.broadCastResponse = broadCastResponse;
    }

    public Response(RequestResponse requestResponse, BroadCastResponse broadCastResponse, Consumer callback) {
        this.requestResponse = requestResponse;
        this.broadCastResponse = broadCastResponse;
        this.callback = callback;
    }

    public RequestResponse getRequestResponse() {
        return requestResponse;
    }

    public BroadCastResponse getBroadCastResponse() {
        return broadCastResponse;
    }

    public void setRequestResponse(RequestResponse requestResponse) {
        this.requestResponse = requestResponse;
    }

    public void setBroadCastResponse(BroadCastResponse broadCastResponse) {
        this.broadCastResponse = broadCastResponse;
    }

    public boolean hasCallback() {
        if (callback == null) return false;
        else return true;
    }

    public Consumer getCallback() {
        return callback;
    }

    public void setCallback(Consumer callback) {
        this.callback = callback;
    }
}
