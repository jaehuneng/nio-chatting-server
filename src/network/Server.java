package network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;

public class Server {
    private static Charset charset = Charset.forName("UTF-8");
    private Network network;
    AsynchronousChannelGroup channelGroup;
    AsynchronousServerSocketChannel serverSocketChannel;

    public void startServer() {
        try {
            network = new Network();

            channelGroup = AsynchronousChannelGroup.withFixedThreadPool(
                    Runtime.getRuntime().availableProcessors(),
                    Executors.defaultThreadFactory()
            );

            serverSocketChannel = AsynchronousServerSocketChannel.open(channelGroup);
            serverSocketChannel.bind(new InetSocketAddress("localhost",5225));

            serverSocketChannel.accept(null,
                    new CompletionHandler<AsynchronousSocketChannel, String>() {
                        @Override
                        public void completed(AsynchronousSocketChannel socketChannel,
                                              String attachment) {
                            try {
                                System.out.println("[연결 수락: " + socketChannel.getRemoteAddress() + ": " + Thread.currentThread().getName() + "]");
                                network.addConnections(socketChannel.getRemoteAddress().toString(), socketChannel);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            serverSocketChannel.accept(null, this);

                            network.receive(socketChannel);
                        }

                        @Override
                        public void failed(Throwable exc, String attachment) {
                            if (serverSocketChannel.isOpen()) stopServer();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopServer() {
        if (channelGroup != null && !channelGroup.isShutdown()) {
            try {
                channelGroup.shutdownNow();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
