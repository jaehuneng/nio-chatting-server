package network;

import protocol.type.RequestType;
import service.Controller;
import buffer.BufferWriter;
import buffer.BufferReader;
import protocol.dto.BroadCastResponse;
import protocol.dto.Request;
import protocol.dto.RequestResponse;
import protocol.dto.Response;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Network {
    private HashMap<String, AsynchronousSocketChannel> connections = new HashMap<>();
    private HashMap<String, List<BroadCastResponse>> broadCastTable = new HashMap<>();

    public void addConnections(String ip, AsynchronousSocketChannel socketChannel) {
        if (connections.containsKey(ip)) {
            connections.replace(ip, socketChannel);
        } else {
            connections.put(ip, socketChannel);
        }
    }

    public void receive(AsynchronousSocketChannel socketChannel) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1000);

        socketChannel.read(byteBuffer, null,
                new CompletionHandler<Integer, Void>() {
                    @Override
                    public void completed(Integer result, Void attachment) {
                        System.out.println(result);
                        if (result == -1) {
                            System.out.println("coasdf");
                            Controller.broadCastSocketDisconnected(findIp(socketChannel));
                        }

                        try {
                            //System.out.println("[요청 처리: " + socketChannel.getRemoteAddress() + ": " + Thread.currentThread().getName() + "]");
                            receive(socketChannel);

                            Request request = BufferReader.readRequestBuffer(byteBuffer);

                            request.setIp(socketChannel.getRemoteAddress().toString());

                            System.out.println("receive: " + request.toString());

                            Response response = Controller.doService(request);

                            send(response);
                        } catch (IOException e) {
                            e.printStackTrace();

                        }
                    }

                    @Override
                    public void failed(Throwable exc, Void attachment) {
                        try {
                            socketChannel.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void send(Response response) {
        RequestResponse requestResponse = response.getRequestResponse();
        BroadCastResponse broadCastResponse = response.getBroadCastResponse();

        if (requestResponse != null) {
            System.out.println("sending: " + requestResponse.toString());
            ByteBuffer byteBuffer = BufferWriter.makeBuffer(requestResponse);

            sendBuffer(connections.get(requestResponse.getIp()), byteBuffer);
        }

        if (broadCastResponse != null) {
            System.out.println("sending: " + broadCastResponse.toString());
            ByteBuffer byteBuffer = BufferWriter.makeBuffer(broadCastResponse);

            List<String> logInUserIps = broadCastResponse.getIpList().stream()
                    .filter(ip -> connections.get(ip) != null)
                    .collect(Collectors.toList());

            List<String> logOutUserIps = broadCastResponse.getIpList().stream()
                    .filter(ip -> connections.get(ip) == null)
                    .collect(Collectors.toList());

            logInUserIps.forEach(ip -> sendBuffer(connections.get(ip), byteBuffer));
            logOutUserIps.forEach(ip -> addBroadCast(ip, broadCastResponse));
        }

        if (response.getRequestResponse().getRequestType() == RequestType.LOGOUT) {
            try {
                connections.get(response.getRequestResponse().getIp()).close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connections.replace(response.getRequestResponse().getIp(), null);
        }
    }

    private void sendBuffer(AsynchronousSocketChannel socketChannel, ByteBuffer byteBuffer) {
        flipBuffer(byteBuffer);

        socketChannel.write(byteBuffer, null,
                new CompletionHandler<Integer, Void>() {
                    @Override
                    public void completed(Integer result, Void attachment) {

                    }

                    @Override
                    public void failed(Throwable exc, Void attachment) {
                        System.out.println(exc.getMessage());

                        try {
                            socketChannel.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void flipBuffer(ByteBuffer buffer) {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        buffer.position(0);
    }

    public void addBroadCast(String userId, BroadCastResponse broadCastResponse) {
        if (broadCastTable.get(userId) == null) {
            broadCastTable.put(userId, new ArrayList<>());
        }

        broadCastTable.get(userId).add(broadCastResponse);
    }

    private String findIp(AsynchronousSocketChannel socketChannel) {
        for (Map.Entry<String, AsynchronousSocketChannel> channel : connections.entrySet()) {
            if (channel.getValue().equals(socketChannel)) {
                return channel.getKey();
            }
        }
        return null;
    }
}
